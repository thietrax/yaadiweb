import React,{ useState,useEffect } from 'react';
import MaterialTable from 'material-table';
import { useSelector, useDispatch } from "react-redux";
import CircularLoading from "../components/CircularLoading";
import {
     editUser, deleteUser
  }  from "../actions/usersactions";

export default function Users() {
  const [data, setData] = useState([]);
  // eslint-disable-next-line
  const [cars, setCars] = useState({});
  const usersdata       = useSelector(state => state.usersdata);
  const cartypes        = useSelector(state => state.cartypes);
  const dispatch        = useDispatch();

  useEffect(()=>{
    if(usersdata.users){
        setData(usersdata.users);
    }
  },[usersdata.users]);

  useEffect(()=>{
    if(cartypes.cars){
        let obj =  {};
        cartypes.cars.map((car)=> obj[car.name]=car.name)
        setCars(obj);
    }
  },[cartypes.cars]);

  const columns = [
      { title: 'Photo profil',             field: 'profile_image',         render: rowData => rowData.profile_image?      <img alt='Profile'        src={rowData.profile_image}   style={{width: 50,borderRadius:'50%'}}/>:null, editable:'never'},
      { title: 'Prénom',                   field: 'firstName',             editable:'never'},
      { title: 'Nom',                      field: 'lastName',              editable:'never'},
      { title: 'User Type',                field: 'usertype',              lookup: { rider: 'Rider', driver: 'Driver', admin: 'Admin' }, editable:'never'},
      { title: 'Email',                    field: 'email',                 editable:'never'},
      { title: 'Téléphone',                field: 'mobile',                editable:'never'},
      { title: 'Modèle Moto',              field: 'vehicleModel',          editable:'never'},
      { title: 'Numéro Moto',              field: 'vehicleNumber',         editable:'never'},
      { title: 'Approuvé le compte',       field: 'approved',              type:'boolean'},
      { title: 'Driver Active Status',     field: 'driverActiveStatus',    type:'boolean'},
      { title: 'Connexion avec ID',        field: 'signupViaReferral',     type:'boolean'},
      { title: 'Compte Mobile Money',      field: 'numeroMoney'                           },
      { title: 'Gains',                    field: 'walletBalance'                           },
      { title: 'Photo Permis',             field: 'licenseImage',          render: rowData => rowData.licenseImage?       <img alt='License'        src={rowData.licenseImage}    style={{width: 100}}/>:null,                   editable:'never'},
      { title: 'Photo Carte Grise',        field: 'carteGriseImage',       render: rowData => rowData.carteGriseImage?    <img alt='CarteGrise'     src={rowData.carteGriseImage} style={{width: 100}}/>:null,                   editable:'never'},
      { title: 'Photo Assurance',          field: 'carteAssuranceImage',   render: rowData => rowData.carteAssuranceImage?<img alt='CarteAssurance' src={rowData.carteGriseImage} style={{width: 100}}/>:null,                   editable:'never'},
    //  { title: 'Bonus',                    field: 'refferalBonus',         type:'numeric', editable:'never'},
    //  { title: 'Refferal Id',              field: 'refferalId',                            editable:'never'}
  ];

  return (
    usersdata.loading? <CircularLoading/>:
    <MaterialTable
      title="Tous les utilisateurs"
      columns={columns}
      data={data}
      options={{
        exportButton: true,
        filtering: true
      }}
      editable={{
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              dispatch(editUser(oldData.id,newData));
            }, 600);
          }),

        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              dispatch(deleteUser(oldData.id));
            }, 600);
          }),   
      }}
    />
  );
}
