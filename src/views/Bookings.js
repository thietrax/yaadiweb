import React,{ useState,useEffect } from 'react';
import MaterialTable from 'material-table';
import CircularLoading from "../components/CircularLoading";
import { useSelector, useDispatch} from "react-redux";
import * as firebase from 'firebase';

import {
  editBooking
}  from "../actions/bookingactions";


export default function Bookings() {



  
       
  var curuser = firebase.auth().currentUser.uid;
  const userData = firebase.database().ref('users/' + curuser);

  //userData.once('value', userData => {
  //    this.setState({
  //        fname: userData.val().firstName,
  //        lname: userData.val().lastName,
  //        email: userData.val().email,
  //        mobile: userData.val().mobile,
  //        numeroMoney: userData.val().numeroMoney,
  //    });
  //})

    const columns =  [
        { title: 'Date réservation',        field: 'tripdate' },
        { title: 'Heure début course',      field: 'trip_start_time' },
        { title: 'Heure fin course',        field: 'trip_end_time' },
        { title: 'Nom Client',              field: 'customer_name'},
       // { title: 'Type Moto',               field: 'carType' },
       // { title: 'Numéro Moto',             field: 'vehicle_number' },  
        { title: 'Adresse ramassage',       field: 'pickupAddress' },
        { title: 'Adresse dépôt',           field: 'dropAddress' },
        {  title: 'Prénom',                 field: 'driverName'},
        { title: 'Statut de réservation',   field: 'status' },
        { title: 'Montant course',          field: 'trip_cost' },
        { title: '% Commission',            field: 'fees'},
        { title: 'Gain Yaadi',              field: 'yaadi_fee'},
        { title: 'Statut de paiement',      field: 'payment_status'},
        { title: 'Mode de paiement',        field: 'payment_mode'},
        ];

        const [data, setData] = useState([]);
        const bookingdata = useSelector(state => state.bookingdata);
        const dispatch = useDispatch();
    


        useEffect(()=>{
              if(bookingdata.bookings){
                setData(bookingdata.bookings);
            }
        },[bookingdata.bookings]);
        return (
          bookingdata.loading? <CircularLoading/>:
          <MaterialTable
            title="Réservations"
            columns={columns}
            data={data}
            options={{
              filtering: true
            }}
            editable={{
              onRowAdd: newData =>
                new Promise(resolve => {
                  setTimeout(() => {
                    resolve();
                    const tblData = data;
                    tblData.push(newData);
                    dispatch(editBooking((tblData),"Add"));
                  }, 600);
                }),
              onRowUpdate: (newData, oldData) =>
                new Promise(resolve => {
                  setTimeout(() => {
                    resolve();
                    const tblData = data;
                    tblData[tblData.indexOf(oldData)] = newData;
                    dispatch(editBooking((tblData),"Update"));
                  }, 600);
                }),
              onRowDelete: oldData =>
                new Promise(resolve => {
                  setTimeout(() => {
                    resolve();
                    const tblData = data;
                    tblData.splice(tblData.indexOf(oldData), 1);
                    dispatch(editBooking((tblData),"Delete"));
                  }, 600);
                }),
            }}
          />
        );
      }
      