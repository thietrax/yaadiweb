import { bookingRef } from "../config/firebase";
import { 
    FETCH_BOOKINGS,
    FETCH_BOOKINGS_SUCCESS,
    FETCH_BOOKINGS_FAILED,
} from "./types";

import  languageJson  from "../config/language";
export const fetchBookings = () => dispatch => {
    dispatch({
      type: FETCH_BOOKINGS,
      payload: null
    });
    bookingRef.on("value", snapshot => {
      if (snapshot.val()) {
        const data = snapshot.val();
        const arr = Object.keys(data).map(i => {
          data[i].id = i
          return data[i]
        });
        dispatch({
          type: FETCH_BOOKINGS_SUCCESS,
          payload: arr
        });
      } else {
        dispatch({
          type: FETCH_BOOKINGS_FAILED,
          payload: languageJson.no_booking_available 
        });
      }
    });
  };

  export const editBooking = (bookings,method) => dispatch =>{
    dispatch({
      type: FETCH_BOOKINGS,
      payload: method
    });
    bookingRef.set(bookings);
  }